import { JwtPayload } from './../../@core/domain/models/jwt-payload.model';
import { AuthService } from './../auth.service';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-jwt';
import { User } from 'src/@core/domain/models/users.model';

@Injectable()
export class jwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: authService.returnJwtExtractor(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(jwtpayload: JwtPayload): Promise<User> {
    const user = await this.authService.validateUser(jwtpayload);

    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
