import { IsNumber, IsString } from 'class-validator';

export class CreateProducts {
  @IsString()
  title: string;

  @IsString()
  description: string;

  @IsString()
  prod_company: string;

  @IsString()
  category: string;

  @IsNumber()
  price: number;

  @IsNumber()
  quantity: number;
}
