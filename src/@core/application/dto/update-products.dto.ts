/* eslint-disable prettier/prettier */
import { PartialType } from '@nestjs/mapped-types';
import { CreateProducts } from './create-products.dto';

export class UpdateProductsDto extends PartialType(CreateProducts) {}
