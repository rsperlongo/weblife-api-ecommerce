/* eslint-disable prettier/prettier */
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
// import { v4 as uuidv4 } from 'uuid';

@Entity('products')
export class ProductsEntity {
  @PrimaryGeneratedColumn('uuid')  
  id: string;

  @Column('varchar')
  title: string;

  @Column('varchar')
  description: string;

  @Column('varchar')
  prod_company: string;

  @Column('varchar')
  category: string;

  @Column('decimal')
  price: number

  @Column('int4')
  quantity: number
}
