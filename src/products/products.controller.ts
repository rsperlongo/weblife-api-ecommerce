import { ProductsEntity } from 'src/@core/domain/products.entity';
import { UpdateProductsDto } from './../@core/application/dto/update-products.dto';
import { CreateProducts } from './../@core/application/dto/create-products.dto';
import {
  Controller,
  Get,
  Post,
  Delete,
  Param,
  Body,
  Patch,
} from '@nestjs/common';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}

  @Post('create')
  async postProducts(@Body() products: CreateProducts) {
    return this.productsService.create(products);
  }

  @Get()
  async getProducts() {
    return this.productsService.findAll();
  }

  @Get('/:id')
  async getByIdProduct(@Param('id') id: string) {
    return this.productsService.findOne(id);
  }

  @Patch('/:id')
  async updateProduct(
    @Param('id') id: string,
    @Body() updateProducts: UpdateProductsDto,
  ): Promise<ProductsEntity> {
    return this.productsService.update(id, updateProducts);
  }

  @Delete('/:id')
  async removeProduct(@Param('id') id: string) {
    return this.productsService.remove(id);
  }
}
