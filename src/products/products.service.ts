import { UpdateProductsDto } from './../@core/application/dto/update-products.dto';
import { CreateProducts } from './../@core/application/dto/create-products.dto';
import { ProductsEntity } from './../@core/domain/products.entity';
import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(ProductsEntity)
    private readonly productsRepo: Repository<ProductsEntity>,
  ) {}

  async findAll() {
    return this.productsRepo.find();
  }

  async findOne(id: string): Promise<ProductsEntity[]> {
    const products = await this.productsRepo.find({ where: { id } });

    if (!products) {
      throw new HttpException(`Product ${id} not found`, HttpStatus.NOT_FOUND);
    }
    return products;
  }

  create(productsDto: CreateProducts) {
    const products = this.productsRepo.create(productsDto);
    return this.productsRepo.save(products);
  }

  async update(id: string, updateProducts: UpdateProductsDto) {
    const products = await this.productsRepo.preload({
      id,
      ...updateProducts,
    });

    if (!products) {
      throw new NotFoundException(`Product ${id} not found`);
    }
    return this.productsRepo.save(products);
  }

  async remove(id: string) {
    const products = await this.productsRepo.findOne({ where: { id } });

    if (!products) {
      throw new NotFoundException(`Product ${id} not found`);
    }
    return this.productsRepo.remove(products);
  }
}
