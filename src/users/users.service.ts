import { SigninDTO } from './../@core/application/dto/signin.dto';
import { AuthService } from './../auth/auth.service';
import { User } from '../@core/domain/models/users.model';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SignupDto } from 'src/@core/application/dto/signup.dto';
import { Model } from 'mongoose';
import { NotFoundException } from '@nestjs/common/exceptions';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User')
    private usersModel: Model<User>,
    private readonly authService: AuthService,
  ) {}

  public async signup(signupDto: SignupDto): Promise<User> {
    const user = new this.usersModel(signupDto);

    return user.save();
  }

  public async signin(
    singinDTO: SigninDTO,
  ): Promise<{ name: string; jwtToken: string; email: string }> {
    const user = await this.findByEmail(singinDTO.email);
    const match = await this.checkPassword(singinDTO.password, user);

    if (!match) {
      throw new NotFoundException('Invalid Credentials');
    }

    const jwtToken = await this.authService.createAccessToken(user._id);

    return { name: user.name, jwtToken, email: user.email };
  }

  public async findAll(): Promise<User[]> {
    return this.usersModel.find();
  }

  private async findByEmail(email: string): Promise<User> {
    const user = await this.usersModel.findOne({ email });

    if (!user) {
      throw new NotFoundException('Email not found');
    }
    return user;
  }

  private async checkPassword(password: string, user: User): Promise<boolean> {
    const match = await bcrypt.compare(password, user.password);

    if (!match) {
      throw new NotFoundException('Password not found');
    }
    return match;
  }
}
