import { UserService } from './users.service';
import { Controller, HttpStatus } from '@nestjs/common';
import {
  Body,
  Get,
  HttpCode,
  Post,
  UseGuards,
} from '@nestjs/common/decorators';
import { User } from 'src/@core/domain/models/users.model';
import { SignupDto } from 'src/@core/application/dto/signup.dto';
import { SigninDTO } from 'src/@core/application/dto/signin.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
export class UserController {
  constructor(private readonly usersService: UserService) {}

  @Post('signup')
  @HttpCode(HttpStatus.CREATED)
  public async signup(@Body() signupDTO: SignupDto): Promise<User> {
    return this.usersService.signup(signupDTO);
  }

  @Post('signin')
  @HttpCode(HttpStatus.OK)
  public async signin(
    @Body() singinDTO: SigninDTO,
  ): Promise<{ name: string; jwtToken: string; email: string }> {
    return this.usersService.signin(singinDTO);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }
}
